Repo Sistemas Operativos

# Comandos de consola

### date -u +"%Y-%m-%d
_Muestra la fecha actual con el formato indicado donde ***%Y*** indica el año, ***%m*** el mes y ***%d*** el dia._
```
root@DebianBrunobzky:/home/brunobzky# date -u +"%Y-%m-%d"
2019-05-07
```

### which $PROGRAMA
_Muestra la ruta absoluta de un programa instalado._
```
root@DebianBrunobzky:/home/brunobzky# which opera
/usr/bin/opera
```

### ps
_Arroja una lista en la que muestra los procesos que estan ejecutandose en el sistema._
```
root@DebianBrunobzky:/home/brunobzky# ps
  PID TTY          TIME CMD
11527 pts/0    00:00:00 sudo
11528 pts/0    00:00:00 su
11534 pts/0    00:00:00 bash
12346 pts/0    00:00:00 ps
```
### history
_Muestra una lista completa de todo el historial de comandos ejecutados en la consola._
```
root@DebianBrunobzky:/home/brunobzky# history 
    1  nano sources.list
    2  nano sources.list 
    3  apt update
    4  wget http://www.deb-multimedia.org/pool/main/d/deb-multimedia-keyring/deb-multimedia-keyring_2016.8.1_all.deb
    5  dpkg -i deb-multimedia-keyring_2016.8.1_all.deb
    6  apt update
    7  apt-get update
    ...
```
### less
_Vizualiza el contenido de un archivo con permisos unicamente de solo lectura._
```
root@DebianBrunobzky:/home/brunobzky# history | less
```
### who
_Muestra los usuarios que están activos._
```
root@DebianBrunobzky:/home/brunobzky# who
brunobzky tty7         2019-05-07 07:24 (:0)
```
### uptime
_Muestra el tiempo que lleva activo el equipo y el numero de usuarios que han interactuado._
```
root@DebianBrunobzky:/home/brunobzky# uptime
 11:59:00 up  4:35,  1 user,  load average: 0.63, 0.61, 0.58
```
### telnet $IP $PORT
_Indica si esta activo o no el puerto indicado, siendo ***$IP*** una direccion ip real y ***$PORT*** el puerto a seleccionar.(Para salir ***Ctrl+] y Ctrl+d*** )_
```
root@DebianBrunobzky:/home/brunobzky# telnet 127.0.0.1 21
Trying 127.0.0.1...
telnet: Unable to connect to remote host: Connection refused
```
### sudo -c $COMANDO
_Se ejecuta un comando en modo usuario root, siendo ***$COMANDO*** la instruccion a ejecutar._
```
brunobzky@DebianBrunobzky:~$  su -c apt install spotify
```
### sudo $COMANDO
_Se ejecuta un comando en modo usuario superusuario, siendo ***$COMANDO*** la instruccion a ejecutar._
```
brunobzky@DebianBrunobzky:~$ su -c apt update
```
### nmap -Pn $IP
_Muestra los puertos que estan activos, siendo ***$IP*** una direccion de ip real._
```
root@DebianBrunobzky:/home/brunobzky# nmap -Pn 127.0.0.1

Starting Nmap 7.40 ( https://nmap.org ) at 2019-05-07 12:14 CDT
Nmap scan report for localhost (127.0.0.1)
Host is up (0.000042s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE
631/tcp  open  ipp
5900/tcp open  vnc

Nmap done: 1 IP address (1 host up) scanned in 1.73 seconds

```
### traceroute $IP
_Crea una prueba de conexion hacia la ip indicada, siendo ***$IP*** una direccion de ip real._
```
root@DebianBrunobzky:/home/brunobzky# traceroute 127.0.0.1
traceroute to 127.0.0.1 (127.0.0.1), 30 hops max, 60 byte packets
 1  localhost (127.0.0.1)  1.937 ms  1.822 ms  1.767 ms
```
### bc
_Se ejecuta un editor de operaciones algebraicas.(Para salir ***Ctrl+d***)_
```
root@DebianBrunobzky:/home/brunobzky# bc
bc 1.06.95
Copyright 1991-1994, 1997, 1998, 2000, 2004, 2006 Free Software Foundation, Inc.
This is free software with ABSOLUTELY NO WARRANTY.
For details type `warranty'. 
33+65-8
90
```
### ping $IP
_Ejecuta una prueba de conexion, siendo  ***$IP*** una ip real.(Para finalizar la tarea ***Ctrl+c***)_
```
bserver@server:~$ ping www.google.com
PING www.google.com (172.217.7.36) 56(84) bytes of data.
64 bytes from qro02s12-in-f36.1e100.net (172.217.7.36): icmp_seq=1 ttl=54 time=16.1 ms
64 bytes from qro02s12-in-f36.1e100.net (172.217.7.36): icmp_seq=2 ttl=54 time=17.9 ms
64 bytes from qro02s12-in-f36.1e100.net (172.217.7.36): icmp_seq=3 ttl=54 time=17.7 ms
^C
--- www.google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 16.199/17.291/17.925/0.783 ms
```
### " | "
_El simbolo ***" |"  pipe***, sirve para enviar un argumento hacia una instruccion._
```
thechrisvazquez@disimil:~$ history | less
```
### " < "
_Extrae el contenido de un archivo._
```
bserver@server:~$ history | less
```
### top
_Muestra un listado de los procesos que se va actualizando dependiendo que proceso esté abarcando mayor cantidad de memoria._
```
bserver@server:~$ top

top - 07:22:43 up 1 day, 19:42,  2 users,  load average: 0.00, 0.00, 0.00
Tasks:  74 total,   1 running,  73 sleeping,   0 stopped,   0 zombie
%Cpu(s):  0.3 us,  0.3 sy,  0.0 ni, 99.3 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
KiB Mem :  1020300 total,   885936 free,    45940 used,    88424 buff/cache
KiB Swap:  1046524 total,  1046524 free,        0 used.   857892 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND      
  626 root      20   0       0      0      0 S  0.3  0.0   0:00.06 kworker/0:2  
  629 root      20   0       0      0      0 S  0.3  0.0   0:00.06 kworker/0:3  
    1 root      20   0   56832   6608   5244 S  0.0  0.6   0:01.08 systemd      
    2 root      20   0       0      0      0 S  0.0  0.0   0:00.00 kthreadd     
    3 root      20   0       0      0      0 S  0.0  0.0   0:00.08 ksoftirqd/0  
    5 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 kworker/0:0H 
    6 root      20   0       0      0      0 S  0.0  0.0   0:00.06 kworker/u2:0 
    7 root      20   0       0      0      0 S  0.0  0.0   0:00.21 rcu_sched    
    8 root      20   0       0      0      0 S  0.0  0.0   0:00.00 rcu_bh       
    9 root      rt   0       0      0      0 S  0.0  0.0   0:00.00 migration/0  
   10 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 lru-add-dra+ 
   11 root      rt   0       0      0      0 S  0.0  0.0   0:00.03 watchdog/0   
   12 root      20   0       0      0      0 S  0.0  0.0   0:00.00 cpuhp/0      
   13 root      20   0       0      0      0 S  0.0  0.0   0:00.00 kdevtmpfs    
   14 root       0 -20       0      0      0 S  0.0  0.0   0:00.00 netns        
   15 root      20   0       0      0      0 S  0.0  0.0   0:00.00 khungtaskd   
   16 root      20   0       0      0      0 S  0.0  0.0   0:00.00 oom_reaper 
```
### sleep $N
_Ejecuta un descanso en  las instrucciones dependiendo la cantidad asignada, siendo ***$N*** el numero de segundos a descansar._
```
bserver@server:~$ sleep 10
```


###### _Bruno Alonso Jauregui Salcedo_